FROM golang:1.16-alpine AS build
WORKDIR /app
COPY go.mod ./
COPY go.sum ./
RUN go mod download
COPY *.go ./
RUN go build -o /events-api

FROM alpine
COPY --from=build /events-api /events-api
ENTRYPOINT ["/events-api"]
